import { Client } from "eris";
import Command from "../interfaces/Command";

export interface Options {
    token: string;
    commands: Command[];
    prefix: string;
};

export default class client {
    client: Client;
    options: Options;
    status?: string;

    constructor(options: Options) {
        this.client = new Client(options.token);
        this.options = options;

        this.registerEvents();
    };

    private registerEvents() {
        this.client.on("messageCreate", async (message) => {
            const { options } = this;
            const [ name, ...args ] = message.content.split(" ");

            if (name.substr(0, options.prefix.length) !== options.prefix) return;

            const commandName = name.substr(options.prefix.length);
            const command = options.commands.find(cmd => cmd.options.name === commandName);
            if (!command || command.options.dm && message.guildID) return;

            command.emit("execute", this.client, message, args);
        });

        this.client.on("ready", () => {
            if (this.status) this.client.editStatus(null, {
                name: this.status
            });
        });
    };

    setStatus(name: string) {
        this.status = name;
    };

    start() {
        this.client.connect();
    };
}