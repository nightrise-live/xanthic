import { Client, EmbedOptions, EmbedField } from "eris";

export const createEmbed = (fields?: EmbedField[], description?: string): EmbedOptions => {
    return {
        title: "nightrise.live",
        description,
        fields,
        color: 0x44c26c
    };
};

export default class display {
    id: string;
    client: Client;
    
    constructor(client: Client, id: string) {
        this.client = client;
        this.id = id;
    };

    async sendAccountInfo(data: any) {
        const fields = [
            {
                name: "id",
                value: data.id,
                inline: true
            },
            {
                name: "name",
                value: data.name,
                inline: true
            },
            {
                name: "enabled",
                value: data.enabled && "true" || "false",
                inline: true
            },
            {
                name: "registered on",
                value: data.createdOn,
                inline: false
            }
        ];

        if (data.discordId) fields.push({
            name: "discord user",
            value: `<@${data.discordId}>`,
            inline: false
        });

        return await this.client.createMessage(this.id, {
            embed: createEmbed(fields)
        });
    };
};