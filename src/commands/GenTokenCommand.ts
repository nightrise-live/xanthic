import Command from "../interfaces/Command";
import { createEmbed } from "../client/display";
import { Message, Client } from "eris";

import Api from "../api";

const command = new Command({
    name: "gentoken",
    dm: true
});

command.on("execute", async (client: Client, message: Message, args: any) => {
    const ctx = new Api(message.author.id);

    const info = await ctx.generateToken(0);
    
    if (!info.success) return client.createMessage(message.channel.id, {
        embed: createEmbed(null, info.error || "There was a error")
    });

    client.addGuildMemberRole(
        process.env.GUILD_ID,
        message.author.id,
        process.env.ROLE_ID
    );

    client.createMessage(message.channel.id, {
        embed: createEmbed(null, `\`${info.code}\``)
    });
});

export default command;