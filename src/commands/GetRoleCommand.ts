import Command from "../interfaces/Command";
import { createEmbed } from "../client/display";
import { Message, Client } from "eris";

import Api from "../api";

const command = new Command({
    name: "getrole",
    dm: true
});

command.on("execute", async (client: Client, message: Message, args: any) => {
    const ctx = new Api(message.author.id);

    const info = await ctx.getDiscordUser();
    
    if (!info.success) return client.createMessage(message.channel.id, {
        embed: createEmbed(null, info.error || "There was a error")
    });

    client.addGuildMemberRole(
        process.env.GUILD_ID,
        message.author.id,
        process.env.ROLE_ID
    );

    client.createMessage(message.channel.id, {
        embed: createEmbed(null, "Successfully gave you the role")
    });
});

export default command;