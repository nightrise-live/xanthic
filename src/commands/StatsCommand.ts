import Command from "../interfaces/Command";
import { createEmbed } from "../client/display";
import { Message, Client } from "eris";
import Api from "../api";
const wrapper = new Api();

const command = new Command({
    name: "stats"
});

command.on("execute", async (client: Client, message: Message, args: any) => {
    const accountStats = await wrapper.getAccountStats();

    if (!accountStats.success) return client.createMessage(message.channel.id, {
        embed: createEmbed(null, "There was a error")
    });

    const fileStats = await wrapper.getFileStats();

    client.createMessage(message.channel.id, {
        embed: createEmbed([
            {
                name: "accounts",
                value: accountStats.accounts,
                inline: true
            },
            {
                name: "files",
                value: fileStats.files,
                inline: true
            },
            {
                name: "size",
                value: fileStats.size,
                inline: true
            }
        ])
    });
});

export default command;