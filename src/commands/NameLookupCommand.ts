import Command from "../interfaces/Command";
import display, { createEmbed } from "../client/display";
import { Message, Client } from "eris";
import Api from "../api";
const wrapper = new Api();

const command = new Command({
    name: "lookup-name"
});

command.on("execute", async (client: Client, message: Message, args: any) => {
    const info = await wrapper.getAccountByName(args[0]);
    const ctx = new display(client, message.channel.id);

    if (!info.success) return client.createMessage(message.channel.id, {
        embed: createEmbed(null, info.error || "There was a error")
    });

    const { account }: { account: any } = info;

    ctx.sendAccountInfo(account);
});

export default command;