import axios, { Method } from "axios";

const URL = process.env.API_URL;
const API_KEY = process.env.ADMIN_KEY;
const BOT_KEY = process.env.BOT_KEY;
const pathRegex = /\//;

import { stringify } from "querystring";

const escapePath = (num: string) => {
    return num.match(pathRegex) ? "0" : num;
};

export default class Wrapper {
    discordId: string;

    constructor(discordId?: string) {
        this.discordId = discordId;
    };

    private async request(url: string, method: Method, data?: any) {
        return axios({
            url,
            baseURL: URL,
            method: method,
            data,
            headers: {
                "Authorization": API_KEY
            }
        });
    };

    private async requestDiscord(url: string, method: Method, data?: any) {
        return axios({
            url,
            baseURL: URL,
            method: method,
            data,
            headers: {
                "Authorization": BOT_KEY,
                "X-DIscord-ID": this.discordId
            }
        });
    };

    private handleError(err: any) {
        if (err.response && err.response.data) return err.response.data;

        return {
            success: false
        };
    };

    async getAccount(uid: string) {
        try {
            const { data } = await this.request(`/admin/account/${escapePath(uid)}`, "GET");

            return data;
        } catch (err) {
            return this.handleError(err)
        }
    };


    async getAccountByName(name: string) {
        try {
            const { data } = await this.request(`/admin/account?${stringify({ name })}`, "GET");

            return data;
        } catch (err) {
            return this.handleError(err);
        };
    };

    async getAccountByDiscord(id: string) {
        try {
            const { data } = await this.request(`/admin/account/discord?id=${encodeURIComponent(id)}`, "GET");

            return data;
        } catch (err) {
            return this.handleError(err);
        };
    };

    async getToken(token: string) {
        try {
            const { data } = await this.request(`/admin/token?${stringify({ token })}`, "GET");

            return data;
        } catch (err) {
            return this.handleError(err);
        };
    };

    async getFileStats() {
        try {
            const { data } = await this.request("/files", "GET");

            return data;
        } catch (err) {
            return this.handleError(err);
        };
    };

    async getAccountStats() {
        try {
            const { data } = await this.request("/accounts", "GET");

            return data;
        } catch (err) {
            return this.handleError(err);
        };
    };

    async linkAccount(token: string) {
        try {
            const { data } = await this.requestDiscord(`/xanthic/link`, "PATCH", { token });

            return data;
        } catch (err) {
            return this.handleError(err);
        };
    };

    async generateToken(type: number) {
        try {
            const { data } = await this.requestDiscord("/xanthic/token/create", "POST", { type });

            return data;
        } catch (err) {
            return this.handleError(err);
        };
    };

    async getDiscordUser() {
        try {
            const { data } = await this.requestDiscord("/xanthic/account/@me", "GET");

            return data;
        } catch (err) {
            return this.handleError(err);
        };
    };
}