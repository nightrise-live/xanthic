import EventEmitter from "events";

export interface Options {
    name: string;
    dm?: boolean | false;
}

export default class Command extends EventEmitter {
    options: Options

    constructor(options: Options) {
        super();

        this.options = options;
    };
}