import { createClient, RedisClient } from "redis";
import { Client } from "eris";
import { createEmbed } from "../client/display";

export default class redisClient {
    redisClient: RedisClient
    client: Client

    constructor(client: Client) {
        this.redisClient = createClient();
        this.client = client;
    };

    subscribe() {
        this.redisClient.on("message", async (channel, message) => {
            if (channel !== "xanthic") return;

            const payload = JSON.parse(message);

            try {
                if (payload.type === 0) {
                    const channel = await this.client.getDMChannel(payload.user);
    
                    await this.client.createMessage(channel.id, {
                        embed: createEmbed(null, payload.message)
                    });
                } else if (payload.type === 1) {
                    await this.client.removeGuildMemberRole(process.env.GUILD_ID, payload.user, process.env.ROLE_ID);
                };
            } catch {};
        });

        this.redisClient.subscribe("xanthic");
    };
}