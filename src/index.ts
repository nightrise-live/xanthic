import { config } from "dotenv";
if (process.env.NODE_EMV !== "production") config();

import Client from "./client";
import LookupCommand from "./commands/LookupCommand";
import NameLookupCommand from "./commands/NameLookupCommand";
import LinkCommand from "./commands/LinkCommand";
import StatsCommand from "./commands/StatsCommand";
import GenTokenCommand from "./commands/GenTokenCommand";
import GetRoleCommand from "./commands/GetRoleCommand";
import LookupDiscordCommand from "./commands/LookupDiscordCommand";

import redisClient from "./redis";

const client = new Client({
    token: process.env.DISCORD_TOKEN,
    prefix: process.env.PREFIX,
    commands: [ LookupCommand, NameLookupCommand, LinkCommand, StatsCommand, GenTokenCommand, GetRoleCommand, LookupDiscordCommand ]
});

client.setStatus(process.env.STATUS);
client.start();

if (process.env.USE_REDIS) new redisClient(client.client).subscribe();